separate-file-test = -> 
  console.log 'separate file works without compiling!'

separate-file-test!


# DANGER: This calls "mymodule.js", not "mymodule.ls"
# calls only possible if the module is browserified
c = require 'mymodule'
c.test "testscript.ls"

d = require 'testmodule'
d.test!