# Overview

This is an example code to show how to modularize a LiveScript project, as [asked in StackOverflow].

Thanks to [Logan F Smyth] for his helps. 


# Instructions 

Run server code: 

    $ lsc server.ls

You will see the proper output:

    this is server script, running with lsc
    my test function has been called

Run client code: 

    $ lsc -c myapp.ls
    $ lsc -c mymodule.ls
    $ browserify -r ./mymodule.js:mymodule > mymodule-browser.js
    $ firefox index.html

Open firebug, you will see the proper output:

    my app is running
    my test function has been called

    
[asked in StackOverflow]: http://stackoverflow.com/questions/26996411/livescripts-require-mymodule-is-not-working-in-browser
[Logan F Smyth]: http://logansmyth.com/
